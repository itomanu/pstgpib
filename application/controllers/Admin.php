<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->database();
    $this->load->library('encrypt');
    $data = array(
      'a_type' => null,
      'a_title' => null,
      'a_body' => null,
      'code' => null,
    );

    $data['code'] = $this->input->get('code');
    
    if ($this->input->post('inputEmail') != null) {
      // Check Code (must be registered)      
      $this->db->where('code', $this->input->post('inputCode'));
      $this->db->where('email', '');
      $result1 = $this->db->get('users'); 
      
      if($result1->num_rows() == 0) {
        $data['a_type'] = 'danger';
        $data['a_title'] = 'Tidak dapat menambahkan User!';
        $data['a_body'] = 'Kode Unik \'' . $this->input->post('inputCode') . '\' tidak ditemukan atau telah digunakan.';
      } else {
        // Check Email (must unique)
        $this->db->where('email', $this->input->post('inputEmail'));
        $result = $this->db->get('users');

        if($result->num_rows() != 0) {
          $data['a_type'] = 'danger';
          $data['a_title'] = 'Tidak dapat menambahkan User!';
          $data['a_body'] = 'Email \'' . $this->input->post('inputEmail') . '\' sudah terdaftar.';
        } else {
          
          $e_salt = $this->encrypt->encode(date("Ymd"));
          $e_pass = $this->encrypt->encode($e_salt . $this->input->post('inputPassword'));

          $array = array(
            'email' => $this->input->post('inputEmail'),
            'name' => $this->input->post('inputName'),
            'user_pass' => $e_pass,
            'user_salt' => $e_salt
          );

          $this->db->where('code', $this->input->post('inputCode'));
          $this->db->update('users', $array);
          
          // If we find a user output correct, else output wrong.
          if ($this->db->affected_rows() == 1) {
            $data['a_type'] = 'success';
            $data['a_title'] = 'Berhasil!';
            $data['a_body'] = 'User berhasil ditambahkan!';
          }
        }
      }
    }

    $this->load->view('admin', $data);
  }

  public function gencode()
  {
    $this->load->database();
    $this->load->helper('string');

    echo '<ol>';

    for ($i=0; $i < 400; $i++) {
      $array = array(
        'code' => random_string('alnum', 5)
      );

      $this->db->insert('users', $array);
      echo '<li>'.$array['code'].'</li>';
      if ($this->db->affected_rows() == 1) {
        echo "success";
      } else {
        echo "error";
      }
    }
    echo '</ol>';
  }
}
