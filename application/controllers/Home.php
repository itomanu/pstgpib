<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('session');
		$data['d_launch'] = "2016/2/15 15:00:00";
		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$data['email'] = $this->session->userdata('email');
    }

		$this->load->view('home', $data);
	}

	public function panitia()
	{
		$this->load->library('session');
		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$data['email'] = $this->session->userdata('email');
    }

		$this->load->view('panitia', $data);
	}

	public function materi()
	{
    $this->load->helper('date');
		$this->load->library('session');
		$this->load->database();
		$data = array('already_login' => false);

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$data['email'] = $this->session->userdata('email');
    }

    $r_materi = $this->db->get('files');
    $data['files'] = $r_materi->result();

    $r_disc = $this->db->get('discusses');
    $data['discusses'] = $r_disc->result();

    $i = 0;
    foreach ($r_disc->result() as $row) {
      $data['discusses'][$i] = $row;
      $datestring = "%d %M, %Y - %h:%i %a";

      $timezone = 'UP7';

      $data['discusses'][$i]->created_at_s = mdate($datestring, gmt_to_local(mysql_to_unix($row->created_at), $timezone));
      $i++;
    }

		$this->load->view('materi', $data);
	}

	public function galeri()
	{
		$this->load->library('session');
		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$data['email'] = $this->session->userdata('email');
    }

		$this->load->view('galeri', $data);
	}
	
	public function gereja()
	{
		$this->load->library('session');
		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$data['email'] = $this->session->userdata('email');
    }

		$this->load->view('gereja', $data);
	}

	public function comment()
	{
		$this->load->database();
		$this->load->library('session');
		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;
  		$id = $this->session->userdata('id');
  		$name = $this->session->userdata('name');

  		$array = array(
        'user_id' => $id,
        'user_name' => $name,
        'comment' => $this->input->post('comment')
      );

      $this->db->insert('discusses', $array);
    }
    
    redirect("/materi");
	}
	
	public function login()
	{
		$this->load->library('session');
		$this->load->database();
    $this->load->library('encrypt');

    if ($this->session->userdata('email') == null) {
	    $data = array(
	      'a_type' => null,
	      'a_title' => null,
	      'a_body' => null
	    );

	    if ($this->input->post('inputEmail') != null) {  
	      $this->db->where('email', $this->input->post('inputEmail'));
	      $result = $this->db->get('users');
	      
	      if ($result->num_rows() != 0) {
	      	$the_user = $result->first_row();
					$c_pass = $the_user->user_salt . $this->input->post('inputPassword');
					$d_pass = $this->encrypt->decode($the_user->user_pass);

	      	if ($d_pass == $c_pass) {
		        $data['a_type'] = 'success';
		        $data['a_title'] = 'Found!';
		        $data['a_body'] = 'Email \'' . $this->input->post('inputEmail') . '\' is logged.';

		        $userdata = array(
		        	'id'    => $the_user->id,
		        	'name'  => $the_user->name,
		          'email' => $the_user->email,
		          'logged_in' => TRUE
		        );

						$this->session->set_userdata($userdata);

						redirect('/materi');

		      } else {
		      	$data['a_type'] = 'danger';
		        $data['a_title'] = 'Warning!';
		        $data['a_body'] = 'Your Email or Password is incorrect.';
		      }
	      } else {
	        
	        $data['a_type'] = 'danger';
	        $data['a_title'] = 'Not Found!';
	        $data['a_body'] = 'Email \''.$this->input->post('inputEmail').'\' not Found!';
	      }
	    }

    	$this->load->view('login', $data);
    } else {
    	redirect('/');
    }
	}

	public function logout()
	{
		$this->load->library('session');
		$data = array('already_login' => false);

    if ($this->session->userdata('email') != null) {
    	$this->session->unset_userdata('id');
    	$this->session->unset_userdata('name');
    	$this->session->unset_userdata('email');
    	$this->session->unset_userdata('logged_in');
    }
		redirect('/');
	}
	
	public function download() {
		$this->load->library('session');
		$this->load->helper('download');
		$this->load->database();

		$data['already_login'] = false;

    if ($this->session->userdata('email') != null) {
  		$data['already_login'] = true;

	    $this->db->where('id', $this->input->get('id'));
    	$r_materi = $this->db->get('files');

    	if ($r_materi->num_rows() != 0) {
    		$file = $r_materi->first_row();

	  		$name = $file->title;
				$dat = file_get_contents("http://pstgpib.org/public/files/$file->file_name");
				$ext = pathinfo($file->file_name, PATHINFO_EXTENSION);

				force_download($name . "." . $ext, $dat);
			} else {
				redirect('/materi');
			}
    } else {
    	redirect('/login');
    }
	}
}
