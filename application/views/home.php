<!DOCTYPE html>
<html>
<head>
	<title>PST GPIB</title>
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?= base_url()?>public/css/style-home.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
	<div class="container-fluid no-padding">
		<div class="head">
			<div class="col-md-8 no-padding slider" id="slideshow">
				<div>
					<img src="<?php echo base_url()?>public/img/web/g1.png"/>
				</div>
				<div>
					<img src="<?php echo base_url()?>public/img/web/g2.png"/>
				</div>
				<div>
					<img src="<?php echo base_url()?>public/img/web/g3.png"/>
				</div>
				<div>
					<img src="<?php echo base_url()?>public/img/web/g4.png"/>
				</div>
				<div>
					<img src="<?php echo base_url()?>public/img/web/g5.png"/>
				</div>

			</div>
			<div class="no-padding welcome">
				<h1 class="title">Selamat datang<br/>di<br/>kota bandung</h1>
				<h1 class="title">Wilujeng sumping</h1>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
	</div>
	<div class="countdown">
    	<h1 style="font-size:2.5em;"class="title"><b>Konven Pendeta GPIB dan Persidangan Sinode Tahunan GPIB</b></h1>
    	<h3 style="font-size:2.0em;"class="title">Bandung, 16 - 20 Februari 2016</h3>

    	<div class="date_countdown">
    		<div id="clock"></div>
			<div class="days col-md-3"><span id="c_d">00</span><br/><span class="accents">Hari</span></div>
			<div class="hours col-md-3"><span id="c_h">00</span><br/><span class="accents">Jam</span></div>
			<div class="minutes col-md-3"><span id="c_m">00</span><br/><span class="accents">Menit</span></div>
			<div class="seconds col-md-3"><span id="c_s">00</span><br/><span class="accents">Detik</span></div>
			<div class="break">&nbsp;</div>
    	</div>
	</div>
	<div class="content">

		<!-- LEFT MENU	-->
		<div class="left_menu col-md-2">
			<div class="fmcircle_out">
				<a href="#">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href=''">Home</span><img onclick="location.href=''"src="<?php echo base_url()?>public/img/web/icon5.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>panitia">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>index.php/panitia'">Panitia</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon4.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>materi">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>index.php/materi'">MATERI</span><img onclick="location.href='<?php echo base_url()?>materi'"src="<?php echo base_url()?>public/img/web/icon3.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>galeri">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>index.php/galeri'">Galeri</span><img onclick="location.href='<?php echo base_url()?>galeri'"src="<?php echo base_url()?>public/img/web/icon2.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>gereja">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>index.php/gereja'">Gereja</span><img onclick="location.href='<?php echo base_url()?>gereja'"src="<?php echo base_url()?>public/img/web/icon1.png" alt="" />
						</div>
					</div>
				</a>
			</div>			
		</div>
		<!-- END OF LEFT MENU	-->
		

		<!-- CONTENT PAGE	-->
		<div class="col-md-7">
			<div class="main_content">
				<center><h2>Persidangan Sinode Tahunan 2016</h2></center>
				
				
				<div class="text-content">
					<h3><u><b>INFO PENTING</u></b></h3>
	
					
					<p style="padding-left:20px"><br>
					Bagi para peserta yang ingin mendownload materi, diharapkan daftar dulu ke Panitia. Bisa menghubungi panitia (Pnt. Agung Suwondo 	/Hp.089699637447 )
					Jika sudah mendapatkan kode unik maka , peserta dapat langsung mendownload materi dengan mengklik menu LOGIN dan mengisi field password(kode unik yang diberikan oleh panitia).
					
					</p>
					
					<br><br><br>
					
					<p style="padding-left:20px">PST GPIB 2016 </p>
				</div>	

				<div style="margin-top:-20px;" class="text-content">
					<h3>Shalom.. Salam Sejahtera</h3>
					<p style="padding-left:20px"><br/>Tanpa terasa guliran waktu terus berputar dan hari pelaksanaan Persidangan Sinode Tahunan Gereja Protestan di
					Indonesia bagian Barat (GPIB) segera akan digelar pada tanggal 16 - 20 Februari 2016
					di Kota Bandung tepatnya di Hotel Papandayan-Bandung.</p>
				</div>	
				
				
				
				
				
				<script type="text/javascript" src="<?php echo base_url(); ?>public/js/jssor.slider-20.min.js"></script>
    <!-- use jssor.slider-20.debug.js instead for debug -->
    <script>
        jssor_1_slider_init = function() {
            
            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Opacity:2}
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 600);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
    </script>

    <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url("<?php echo base_url()?>public/img/web/b05.png") no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 12 css */
        /*
        .jssora12l                  (normal)
        .jssora12r                  (normal)
        .jssora12l:hover            (normal mouseover)
        .jssora12r:hover            (normal mouseover)
        .jssora12l.jssora12ldn      (mousedown)
        .jssora12r.jssora12rdn      (mousedown)
        */
        .jssora12l, .jssora12r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 30px;
            height: 46px;
            cursor: pointer;
            background: url("<?php echo base_url()?>public/img/web/a12.png") no-repeat;
            overflow: hidden;
        }
        .jssora12l { background-position: -16px -37px; }
        .jssora12r { background-position: -75px -37px; }
        .jssora12l:hover { background-position: -136px -37px; }
        .jssora12r:hover { background-position: -195px -37px; }
        .jssora12l.jssora12ldn { background-position: -256px -37px; }
        .jssora12r.jssora12rdn { background-position: -315px -37px; }
    </style>


    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url("<?php echo base_url()?>public/img/web/loading.gif") no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/daftar1.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/daftar2.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/cr3.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/rp3.jpg" />
            </div>
			 <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/cr7.jpg" />
            </div>
			 <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/kegiatan/cr6.jpg" />
            </div>
			 <div style="display: none;">
                <img data-u="image" src="<?php echo base_url()?>public/img/web/logo1.jpg" />
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:6px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora12l" style="top:123px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora12r" style="top:123px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
        <a href="http://www.jssor.com" style="display:none">Jssor Slider</a>
    </div>
    <script>
        jssor_1_slider_init();
    </script>
				
				<div class="text-content">
					
					<p style="padding-left:20px"><br/>Selamat datang di website resmi Panitia Pelaksana Konven Pendeta & PST GPIB Tahun 2016.
					Media komunikasi on-line ini dibuat oleh panitia sebagai alternatif sumber informasi resmi mengenai penyelenggaraan kegiatan tersebut diatas.
					<br><br>
				
					Kami sekedar mengakomodasi perilaku warga GPIB, yang semakin agresif dan kreatif dalam mendapatkan informasi berkualitas.
					
					
					Dengan demikian maka bukan sekedar <i>branding</i>, melainkan kemudahan informasi berkualitas sekaligus sosialisasi kegiatan. Itu yang menjadi objective pembuatan website ini.
					
					<br><br>
					Semoga menjadi berkat.<br><br>
					
					
					
					

					
					
						Salam<br/><br/><br/>
						<strong><u>Ronald K.Kumowal</u></strong><br>
						<i>Ketua Panitia Pelaksana</i><br>
						<i>Konven Pendeta & PST GPIB 2016</i>
					
					
					</p>
		
					<br>
					<center><strong><p> Kata Sambutan KETUA BP MUPEL JABAR I </p></strong></center>
					<div>
						<iframe width="100%" height="315" src="https://www.youtube.com/embed/WpdCe735xuk" frameborder="0" allowfullscreen></iframe>
					</div>
					
					
					
					
					<br><br>
					
					
					
					<center><p style="margin-bottom:-40px;">Copyright @Panpel PST Web Developer 2016 </p></center>
					

					<div class="break">&nbsp;</div>
				</div>
			</div>
		</div>
		<!-- END OF CONTENT PAGE-->
		


		<!-- RIGHT MENU	-->
		<div class="right_menu col-md-3">
			<div class="content_menu">
				<div class="menu_header">Menu</div>
				<div class="menu_content">
					<ul>
            <?php if (!$already_login) {
						  echo "<center><li><a style='font-size:1.4em;' href='".base_url()."login' class='link'>Login</a></li>	</center>";
					  } else {
              echo "<center><li><a style='font-size:1.4em;' href='".base_url()."logout' class='link'>Logout</a></li>  </center>";
            } ?>
          </ul>

				</div>
			</div>
			<div class="content_menu">
			<center>
				<div class="menu_header">Media Sosial</div>
				<div class="menu_content">
				
				<a href= "https://www.facebook.com/profile.php?id=100009224248313" >
				<img  style ="width:220px; height:200px;" src="<?php echo base_url()?>public/img/web/fb.jpg">
			</center>
				</a>
				
				</div>
			</div>
		</div>
		<!-- END OF RIGHT MENU -->
		
		<!-- <div class="break">&nbsp;</div> -->
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/jquery.countdown.min.js"></script>
    <script type="text/javascript">
    	$('#clock').countdown("<?php echo $d_launch ?>")
		    .on('update.countdown', function(event) {
					$('#c_d').html(event.strftime('%-D'));
					$('#c_h').html(event.strftime('%H'));
					$('#c_m').html(event.strftime('%M'));
					$('#c_s').html(event.strftime('%S'));
		    })
		    .on('finish.countdown', function(event) {
		    	
		    });

	    $("#slideshow > div:gt(0)").hide();

			setInterval(function() { 
			  $('#slideshow > div:first')
			    .fadeOut(1000)
			    .next()
			    .fadeIn(1000)
			    .end()
			    .appendTo('#slideshow');
			},  3000);
    </script>
</body>
</html>