<!DOCTYPE html>
<html>
<head>
	<title>Galeri PST GPIB</title>
	<link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/>   
    <link href="<?= base_url()?>public/css/style-other.css" rel="stylesheet" type="text/css" media="all" />
	
	<style>
	p{
		font-size:1.4em;
		
	}
	</style>

</head>
<body>
	
	<div class="content">
	
		<!-- LEFT MENU	-->
		<div class="left_menu col-md-2">
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Home</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon5.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>panitia">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Panitia</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon4.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>materi">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>materi'">MATERI</span><img onclick="location.href='<?php echo base_url()?>materi'"src="<?php echo base_url()?>public/img/web/icon3.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="#">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href=''">Galeri</span><img onclick="location.href=''"src="<?php echo base_url()?>public/img/web/icon2.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>gereja">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>gereja'">Gereja</span><img onclick="location.href='<?php echo base_url()?>gereja'"src="<?php echo base_url()?>public/img/web/icon1.png" alt="" />
						</div>
					</div>
				</a>
			</div>			
		</div>
		<!-- END OF LEFT MENU	-->
		
		<div class="center_menu">
		<img  style="margin-top:-6px;margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_f.png"/>
		
		<p style="padding-left:30px;"> Kegiatan dan Rapat Pleno PST GPIB </p>
		
		<style>
		#images-box {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;
			margin-bottom:240px;
			position: relative;			
		}
		
		#images-box1 {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;			
			position: relative;			
		}
		
		#images-box2 {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;
			margin-top:200px;
			position: relative;			
		}
		
		#images-box3 {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;
			margin-top:200px;
			position: relative;			
		}
		
		#images-box4 {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;
			margin-top:200px;
			position: relative;			
		}
		
		.image-lightbox img {
			/* Inherit the width and height from the parent element */
			width: inherit;
			height: inherit;
			z-index: 3000;
		}

		.holder {
			/* The width and height, you can change these */
			width: 250px;
			height: 166px;
			/* Float left, so everything aligns right */
			float: left;
			margin-bottom:25px;
			margin-right:-20px;
		}

		.image-lightbox {
			/* Inherit width and height from the .holder */
			width: inherit;
			height: inherit;
			padding: 10px;
			/* Box shadow */
			box-shadow: 0px 0px 10px rgba(0,0,0,0.1);
			background: #fff;
			border-radius: 5px;
			/* Position absolutely so we can zoom it out later */
			position: absolute;
			top: 0;
			font-family: Arial, sans-serif;
			/* Transitions to provide some eye candy */
			-webkit-transition: all ease-in 0.5s;
			-moz-transition: all ease-in 0.5s;
			-ms-transition: all ease-in 0.5s;
			-o-transition: all ease-in 0.5s;
		}

		.image-lightbox span {
			display: none;
		}

		.image-lightbox .expand {
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0;
			z-index: 4000;
			background: rgba(0,0,0,0); /* Fixes an IE bug */
			left: 0;
		}

		.image-lightbox .close {
			position: absolute;
			width: 20px; height: 20px;
			right: 20px; top: 20px;
		}

		.image-lightbox .close a {
			height: auto; width: auto;
			padding: 5px 10px;
			color: #fff;
			text-decoration: none;
			background: #22272c;
			box-shadow: inset 0px 24px 20px -15px rgba(255, 255, 255, 0.1), inset 0px 0px 10px rgba(0,0,0,0.4), 0px 0px 30px rgba(255,255,255,0.4);
			border-radius: 5px;
			font-weight: bold;
			float: right;
		}

		.image-lightbox .close a:hover {
			box-shadow: inset 0px -24px 20px -15px rgba(255, 255, 255, 0.01), inset 0px 0px 10px rgba(0,0,0,0.4), 0px 0px 20px rgba(255,255,255,0.4);
		}

		div[id^=image]:target {
			width: 630px;
			height: 450px;
			z-index: 5000;
			top: 50px;
			left: 200px;
		}

		div[id^=image]:target .close {
			display: block;
		}
		 
		div[id^=image]:target .expand {
			display: none;
		}
		
		
div#image-1 { left: 0; }
div#image-2 { left: 290px; }
div#image-3 { left: 580px; }
div#image-4 { left: 0; }
div#image-5 { left: 290px; }
div#image-6 { left: 580px; }
div#image-7 { left: 0; }
div#image-8 { left: 290px; }
div#image-9 { left: 580px; }
div#image-10 { left: 0; }
div#image-11 { left: 290px; }
div#image-12 { left: 580px; }
div#image-13 { left: 0; }
div#image-14 { left: 290px; }
div#image-15 { left: 580px; }
 
div#image-1:target, div#image-2:target, div#image-3:target { left: 200px; }
div#image-4:target, div#image-5:target, div#image-6:target { left: 200px; }
div#image-7:target, div#image-8:target, div#image-9:target { left: 200px; }
div#image-10:target, div#image-11:target, div#image-12:target { left: 200px; }
div#image-13:target, div#image-14:target, div#image-15:target { left: 200px; }
		</style>
		
		
	<!------------------------------------------------------------------------------------------------>	
	<div id="images-box">
		<div class="holder">
        <div id="image-1" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst7.jpg" alt="">
            <a class="expand" href="#image-1"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-2" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst12.jpg" alt="">
            <a class="expand" href="#image-2"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-3" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst1.jpg" alt="">
            <a class="expand" href="#image-3"></a>
			</div>
		</div>		
	</div>
	<!------------------------------------------------------------------------------------------------>
		
		
	<div id="images-box1">
		<div class="holder">
        <div id="image-4" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst2.jpg" alt="">
            <a class="expand" href="#image-4"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-5" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst3.jpg" alt="">
            <a class="expand" href="#image-5"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-6" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst4.jpg" alt="">
            <a class="expand" href="#image-6"></a>
			</div>
		</div>
	</div>
	<!------------------------------------------------------------------------------------------------>
	<br>
		
	<div id="images-box2">
		<div class="holder">
        <div id="image-7" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst5.jpg" alt="">
            <a class="expand" href="#image-7"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-8" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst6.jpg" alt="">
            <a class="expand" href="#image-8"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-9" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst8.jpg" alt="">
            <a class="expand" href="#image-9"></a>
			</div>
		</div>
	</div>
	<!------------------------------------------------------------------------------------------------>

	<br>
		
	<div id="images-box3">
		<div class="holder">
        <div id="image-10" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst9.jpg" alt="">
            <a class="expand" href="#image-10"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-11" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst10.jpg" alt="">
            <a class="expand" href="#image-11"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-12" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/pst11.jpg" alt="">
            <a class="expand" href="#image-12"></a>
			</div>
		</div>
	</div>
		
		
	<!------------------------------------------------------------------------------------------------>

	<br>
		
	<div id="images-box4">
		<div class="holder">
        <div id="image-13" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/cr1.jpg" alt="">
            <a class="expand" href="#image-13"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-14" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/cr5.jpg" alt="">
            <a class="expand" href="#image-14"></a>
			</div>
		</div>
		
		<div class="holder">
        <div id="image-15" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/kegiatan/rk2.jpg" alt="">
            <a class="expand" href="#image-15"></a>
			</div>
		</div>
	</div>	
	<!------------------------------------------------------------------------------------------------>
		
		
		<img  style="margin-top:20px;margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_v.png"/>
		
		<p style="padding-left:30px;"> Sambutan Panitia Konven Pendeta dan PST GPIB </p>
		
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;padding-right:40px;" src="https://www.youtube.com/embed/yXMlNy1FMBg" frameborder="0" allowfullscreen></iframe>
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;" src="https://www.youtube.com/embed/J_FdAYb3C7Y" frameborder="0" allowfullscreen></iframe>
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;padding-right:40px;" src="https://www.youtube.com/embed/gZ23I4n-l4Y" frameborder="0" allowfullscreen></iframe>
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;" src="https://www.youtube.com/embed/dSQlFcKacy0" frameborder="0" allowfullscreen></iframe>	
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;padding-right:40px;" src="https://www.youtube.com/embed/HpaLCx0WAP0" frameborder="0" allowfullscreen></iframe>
		<iframe width="400" height="315" style="padding-top:20px;padding-left:20px;" src="https://www.youtube.com/embed/be6Odk58eMs" frameborder="0" allowfullscreen></iframe>				
				
		
		</div>
		
		<div class="right_menu">
		<div class="content_menu">
				<div class="menu_header">Menu</div>
				<div class="menu_content">
					<ul>
						<?php if (!$already_login) {
						  echo "<center><li><a style='font-size:1.4em;' href='".base_url()."login' class='link'>Login</a></li>	</center>";
					  } else {
              echo "<center><li><a style='font-size:1.4em;' href='".base_url()."logout' class='link'>Logout</a></li>  </center>";
            } ?>
					</ul>

				</div>
			</div>
			<div class="content_menu">
			<center>
				<div class="menu_header">Media Sosial</div>
				<div class="menu_content">
				<img onclick="location.href='https://www.facebook.com/profile.php?id=100009224248313'" style ="padding-left:-20px;width:180px; height:180px;" src="<?php echo base_url()?>public/img/web/fb.jpg"/>
				</div>
			</center>
			</div>
		</div>
		
		
	</div>

    
</body>
</html>