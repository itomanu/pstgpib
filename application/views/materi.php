<!DOCTYPE html>
<html>
<head>
	<title>Materi PST GPIB</title>
	<link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/> 
  <link href="<?= base_url()?>public/css/style-other.css" rel="stylesheet" type="text/css" media="all" />
 	<style>
 	@import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
		tr{
			font-size:1.3em;
		}
	 	
	 	p{
		  font-size:1.3em;
	 	}

	 	.comm {
	 		width: 95%;
	 	}

	 	.panel-body {
	 		border: 1pt #999 solid;
	    padding: 10px;
	    border-radius: 5px;
	    margin-bottom: 6px;
	 	}

	 	textarea.form-control {
		    height: auto;
		}
		.form-control {
		    display: block;
		    width: 92%;
		    height: 34px;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    color: #555;
		    background-color: #fff;
		    background-image: none;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
		    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		}
		textarea {
		    width: auto;
		}
		button, input, select, textarea {
		    font-family: inherit;
		    font-size: inherit;
		    line-height: inherit;
		}
		textarea {
		    overflow: auto;
		}
		button, input, optgroup, select, textarea {
		    margin: 0;
		    font: inherit;
		    color: inherit;
		}

	</style>
</head>
<body>
	
	<div class="content">
	
		<!-- LEFT MENU	-->
		<div class="left_menu col-md-2">
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Home</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon5.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>panitia">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Panitia</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon4.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="#">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href=''">MATERI</span><img onclick="location.href=''"src="<?php echo base_url()?>public/img/web/icon3.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>galeri">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>galeri'">Galeri</span><img onclick="location.href='<?php echo base_url()?>galeri'"src="<?php echo base_url()?>public/img/web/icon2.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>gereja">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>gereja'">Gereja</span><img onclick="location.href='<?php echo base_url()?>gereja'"src="<?php echo base_url()?>public/img/web/icon1.png" alt="" />
						</div>
					</div>
				</a>
			</div>			
		</div>
		<!-- END OF LEFT MENU	-->
		
		<div class="center_menu">
			<img  style="margin-top:-6px;margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_m.png"/>
				<?php
					if ($already_login) {
						echo
						 "<center>
						 		<p> Selamat Datang, $email</p>
								<a href='logout' class='link'>logout</a>
							</center>";
					} else {
						echo
							"<center><p> Login Untuk Dapat Mendownload Materi Acara </p>
								<a href='login' class='link'>Login</a>
							</center>";
					}?>

						<div class="text-content">
							<p class="text-center"><strong>Daftar File</strong></p>

							<table width="100%" >
								<tbody>
									<tr>
										<td style="text-align: left;" width="5%"><strong>No</strong></td>
										<td style="text-align: left;" width="80%"><strong>Nama File</strong></td>
										<td style="text-align: left;" width="15%"><strong>Action</strong></td>
									</tr>
									<?php
										$i = 1;
										foreach ($files as $row)
										{
											echo
											"<tr>
												<td width='5%'>$i</td>
												<td width='80%'>$row->title</td>
												<td width='15%'><a href='download?id=$row->id' target='_blank'>Download</a></td>
											</tr>";
											$i++;
										}
									?>
								</tbody>
							</table>
						</div>

						<div class="text-content">
							<p class="text-center"><strong>Saran & Komentar</strong></p>
							<div class="comm">
								<?php
								foreach ($discusses as $row)
								{
									echo "
										<div class='panel-body'>
		                  <div class='text-left'>
		                    <div class='comment-user'><i class='fa fa-user'></i> $row->user_name</div>
		                    <time class='comment-date' datetime='$row->created_at'><i class='fa fa-clock-o'></i> $row->created_at_s</time>
		                  </div>
		                  <div>
		                    <p>
		                      $row->comment
		                    </p>
		                  </div>
		                </div>";
	              }
                ?>
              </div>

              <div class="widget-area no-padding blank">
								<div class="status-upload">
									<form action="comment" method="post">
										<textarea class="form-control" name="comment" placeholder="Kirimkan komentar Anda!" ></textarea>
										<button type="submit" class="btn btn-success green">Kirim</button>
									</form>
								</div><!-- Status Upload  -->
							</div><!-- Widget Area -->
						</div>
		</div>
		
		<div class="right_menu">
		<div class="content_menu">
				<div class="menu_header">Menu</div>
				<div class="menu_content">
					<ul>
					<?php if (!$already_login) {
						  echo "<center><li><a style='font-size:1.4em;' href='".base_url()."login' class='link'>Login</a></li>	</center>";
					  } else {
              echo "<center><li><a style='font-size:1.4em;' href='".base_url()."logout' class='link'>Logout</a></li>  </center>";
            } ?>
					</ul>

				</div>
			</div>
			<div class="content_menu">
			<center>
				<div class="menu_header">Media Sosial</div>
				<div class="menu_content">
				<img onclick="location.href='https://www.facebook.com/profile.php?id=100009224248313'" style ="padding-left:-20px;width:180px; height:180px;" src="<?php echo base_url()?>public/img/web/fb.jpg"/>
				</div>
			</center>
			</div>
		</div>
		
		
	</div>

    
</body>
</html>