<!DOCTYPE html>
<html>
<head>
	<title>Mupel Jabar I PST GPIB</title>
	<link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/>
    <link href="<?= base_url()?>public/css/style-other.css" rel="stylesheet" type="text/css" media="all" />
	
	<style>
	p{
		font-size:1.7em;
	}
	</style>
</head>
<body>
	
	<div class="content">
	
		<!-- LEFT MENU	-->
		<div class="left_menu col-md-2">
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Home</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon5.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>panitia">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Panitia</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon4.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>materi">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>materi'">MATERI</span><img onclick="location.href='<?php echo base_url()?>materi'"src="<?php echo base_url()?>public/img/web/icon3.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>galeri">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>galeri'">Galeri</span><img onclick="location.href='<?php echo base_url()?>galeri'"src="<?php echo base_url()?>public/img/web/icon2.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="#">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href=''">Gereja</span><img onclick="location.href=''"src="<?php echo base_url()?>public/img/web/icon1.png" alt="" />
						</div>
					</div>
				</a>
			</div>			
		</div>
		<!-- END OF LEFT MENU	-->
		
		<div class="center_menu">
		<img  style="margin-top:-6px;margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_lg.png"/>
		
		<center><p> Gereja GPIB Mupel Jabar 1</p></center>
		
		
		<style>
		#images-box {
		/* The total width of the image-box, mainly for centering */
			width: 850px;
			margin-left:20px;
			margin-bottom:240px;
			position: relative;			
		}
		
		
		.image-lightbox img {
			/* Inherit the width and height from the parent element */
			width: inherit;
			height: inherit;
			z-index: 3000;
		}

		.holder {
			/* The width and height, you can change these */
			width: 250px;
			height: 166px;
			/* Float left, so everything aligns right */
			float: left;
			margin-bottom:25px;
			margin-right:-20px;
		}

		.image-lightbox {
			/* Inherit width and height from the .holder */
			width: inherit;
			height: inherit;
			padding: 10px;
			/* Box shadow */
			box-shadow: 0px 0px 10px rgba(0,0,0,0.1);
			background: #fff;
			border-radius: 5px;
			/* Position absolutely so we can zoom it out later */
			position: absolute;
			top: 0;
			font-family: Arial, sans-serif;
			/* Transitions to provide some eye candy */
			-webkit-transition: all ease-in 0.5s;
			-moz-transition: all ease-in 0.5s;
			-ms-transition: all ease-in 0.5s;
			-o-transition: all ease-in 0.5s;
		}

		.image-lightbox span {
			display: none;
		}

		.image-lightbox .expand {
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0;
			z-index: 4000;
			background: rgba(0,0,0,0); /* Fixes an IE bug */
			left: 0;
		}

		.image-lightbox .close {
			position: absolute;
			width: 20px; height: 20px;
			right: 20px; top: 20px;
		}

		.image-lightbox .close a {
			height: auto; width: auto;
			padding: 5px 10px;
			color: #fff;
			text-decoration: none;
			background: #22272c;
			box-shadow: inset 0px 24px 20px -15px rgba(255, 255, 255, 0.1), inset 0px 0px 10px rgba(0,0,0,0.4), 0px 0px 30px rgba(255,255,255,0.4);
			border-radius: 5px;
			font-weight: bold;
			float: right;
		}

		.image-lightbox .close a:hover {
			box-shadow: inset 0px -24px 20px -15px rgba(255, 255, 255, 0.01), inset 0px 0px 10px rgba(0,0,0,0.4), 0px 0px 20px rgba(255,255,255,0.4);
		}

		div[id^=image]:target {
			width: 630px;
			height: 450px;
			z-index: 5000;
			top: 50px;
			left: 200px;
		}

		div[id^=image]:target .close {
			display: block;
		}
		 
		div[id^=image]:target .expand {
			display: none;
		}
		
		
div#image-1 { left: 0; }
div#image-2 { left: 0; }
div#image-3 { left: 0; }
div#image-4 { left: 0; }


 
div#image-1:target, div#image-2:target, div#image-3:target { left: 200px; }

		</style>
		
		<div id="images-box">
		<div class="holder">
        <div id="image-1" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja1.JPG" alt="">
            <a class="expand" href="#image-1"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Bethel Bandung </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Jl. Wastukencana No. 1, Jawa Barat 40117
		<br>
		Link &nbsp &nbsp &nbsp &nbsp:<a style="margin-top:-20px;font-size:1em;" target="_blank" href="https://gpibbethel.wordpress.com/2010/02/10/gpib-bethel-bandung/">gpibbethel.wordpress.com/2010/02/10/gpib-bethel-bandung/ </a>
		
		<!---------------------------------------------------------------------------------------!-->
		
		<div id="images-box">
		<div class="holder">
        <div id="image-2" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja2.JPG" alt="">
            <a class="expand" href="#image-2"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Sejahtera Bandung </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Jl. Malabar No.49-51, Lengkong, Kota Bandung, Jawa Barat 40263
		<br>
		Link &nbsp &nbsp &nbsp &nbsp:<a style="margin-top:-20px;font-size:1em;" target="_blank" href="http://gpibsejahterabdg.com/">gpibsejahterabdg.com/ </a>
		
		<!---------------------------------------------------------------------------------------!-->
		
		<div id="images-box">
		<div class="holder">
        <div id="image-3" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja3.JPG" alt="">
            <a class="expand" href="#image-3"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Maranatha Bandung </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Jalan Taman Cibeunying Utara No.2, Jawa Barat 40114
		<br>
		Link &nbsp &nbsp &nbsp &nbsp:<a style="margin-top:-20px;font-size:1em;" target="_blank" href="https://gpibmaranathabdg.wordpress.com/sekilas-gpib-maranatha-bandung/">gpibmaranathabdg.wordpress.com/sekilas-gpib-maranatha-bandung/</a>
		
		<!---------------------------------------------------------------------------------------!-->
		
		
		
		
		<div id="images-box">
		<div class="holder">
        <div id="image-4" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja4.JPG" alt="">
            <a class="expand" href="#image-4"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Wisma Asih Lembang </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Jl. Maribaya No.93, Lembang, Jawa Barat 40391
		<br>
		Link &nbsp &nbsp &nbsp &nbsp:<a style="margin-top:-20px;font-size:1em;" target="_blank" href="http://gpibwismaasihlembang.blogspot.co.id/">gpibwismaasihlembang.blogspot.co.id/ </a>
		
		<!---------------------------------------------------------------------------------------!-->
		
			<div id="images-box">
		<div class="holder">
        <div id="image-5" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja5.JPG" alt="">
            <a class="expand" href="#image-5"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Immanuel Cimahi </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Jl. Pasir Kumeli 149 A, Baros, Kota Cimahi.
		<br>
		Link &nbsp &nbsp &nbsp &nbsp:<a style="margin-top:-20px;font-size:1em;" target="_blank" href="http://www.gpibimanuelcimahi.org">gpibimanuelcimahi.org </a>
		
		<!---------------------------------------------------------------------------------------!-->
		
			<div id="images-box">
		<div class="holder">
        <div id="image-6" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja6.JPG" alt="">
            <a class="expand" href="#image-6"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Silih Asih Bandung </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:Komplek Yonzipur 3, JL Moch. R. Djayadinata, No. 3, Bojongasih, Dayeuhkolot
		<br>
		Link &nbsp &nbsp &nbsp &nbsp: <a style="margin-top:-20px;font-size:1em;" target="_blank" href="http://gpibsilihasihbandung.org/">http://gpibsilihasihbandung.org/</a>
		
		<!---------------------------------------------------------------------------------------!-->
		
			<div id="images-box">
		<div class="holder">
        <div id="image-7" class="image-lightbox">
            <span class="close"><a href="#">X</a></span>
            <img src="<?php echo base_url()?>public/img/web/gereja7.JPG" alt="">
            <a class="expand" href="#image-7"></a>
			</div>
		</div>
		</div>
		
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Gereja &nbsp &nbsp :GPIB Pniel Dayeuh Kolot </p>
		<p style="margin-top:-20px;margin-left:20px;font-size:1.2em;">Alamat &nbsp &nbsp:JL.Komplek Yonzipur 3, JL Moch. R. Djayadinata, No. 3, Bojongasih, Dayeuhkolot
		<br>

		
		<!---------------------------------------------------------------------------------------!-->
		
		
		
		<br><br><br><br>
		
						
		</div>
		
		<div class="right_menu">
		<div class="content_menu">
				<div class="menu_header">Menu</div>
				<div class="menu_content">
					<ul>
					<?php if (!$already_login) {
						  echo "<center><li><a style='font-size:1.4em;' href='".base_url()."login' class='link'>Login</a></li>	</center>";
					  } else {
              echo "<center><li><a style='font-size:1.4em;' href='".base_url()."logout' class='link'>Logout</a></li>  </center>";
            } ?>
					</ul>

				</div>
			</div>
			<div class="content_menu">
			<center>
				<div class="menu_header">Media Sosial</div>
				<div class="menu_content">
				<img onclick="location.href='https://www.facebook.com/profile.php?id=100009224248313'" style ="padding-left:-20px;width:180px; height:180px;" src="<?php echo base_url()?>public/img/web/fb.jpg"/>
				</div>
			</center>
			</div>
		</div>
		
		
	</div>

    
</body>
</html>