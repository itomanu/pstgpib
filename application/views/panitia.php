<!DOCTYPE html>
<html>
<head>
	<title>Panitia PST GPIB</title>
	<link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/>
	<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
	<style>
	tr{
		font-size:1.3em;
	}
	p{
		font-size:1.3em;
	}
	</style>
	
  <link href="<?= base_url()?>public/css/style-other.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
	
	<div class="content">
	
		<!-- LEFT MENU	-->
		<div class="left_menu col-md-2">
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>panitia'">Home</span><img onclick="location.href='<?php echo base_url()?>panitia'"src="<?php echo base_url()?>public/img/web/icon5.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="#">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href=''">Panitia</span><img onclick="location.href=''"src="<?php echo base_url()?>public/img/web/icon4.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>materi">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>materi'">MATERI</span><img onclick="location.href='<?php echo base_url()?>materi'"src="<?php echo base_url()?>public/img/web/icon3.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>galeri">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>galeri'">Galeri</span><img onclick="location.href='<?php echo base_url()?>galeri'"src="<?php echo base_url()?>public/img/web/icon2.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			
			<div class="fmcircle_out">
				<a href="<?php echo base_url()?>gereja">
					<div class="fmcircle_border">
						<div class="fmcircle_in">
							<span onclick="location.href='<?php echo base_url()?>gereja'">Gereja</span><img onclick="location.href='<?php echo base_url()?>gereja'"src="<?php echo base_url()?>public/img/web/icon1.png" alt="" />
						</div>
					</div>
				</a>
			</div>			
		</div>
		<!-- END OF LEFT MENU	-->
		
		<div class="center_menu">
			<img style="margin-top:-6px;margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_sp.png"/>
			<div class="text-content">
				<p class="text-center"><strong>Sesuai Dengan SK Majelis Sinode GPIB Nomor 5283/IX-15/MS/XIX/Kpts</strong></p>

				<table width="100%" >
					<tbody>
						<tr>
							<td style="text-align: left;" width="38"><strong>No</strong></td>
							<td style="text-align: left;" width="217"><strong>Jabatan</strong></td>
							<td style="text-align: left;" width="255"><strong>Nama</strong></td>
							<td style="text-align: left;" width="80"><strong>Jemaat</strong></td>
							
						</tr>
						<tr>
							<td width="38"></td>
							<td width="217">Penangggung-jawab</td>
							<td width="255">MAJELIS SINODE GPIB</td>
							<td width="80"></td>
						
						</tr>
						<tr>
							<td width="38">001</td>
							<td width="217">Penasehat</td>
							<td width="255">Pdt. Ebser M. Lalenoh, M.Th.</td>
							<td width="80">Maranatha Bandung</td>
						
						</tr>
						<tr>
							<td width="38">002</td>
							<td width="217">Penasehat</td>
							<td width="255">Mayjend TNI.(Purn) Leonard Harjono</td>
							<td width="80">Sejahtera Bandung</td>
							
						</tr>
						<tr>
							<td width="38">003</td>
							<td width="217">Penasehat</td>
							<td width="255">Penatua DR. Marthen Tapilouw</td>
							<td width="80">Bethel Bandung</td>
							
						</tr>
							<tr>
							<td width="38">004</td>
							<td width="217">KETUA UMUM</td>
							<td width="255">Pnt. Drs. Ronald K. Kumowal SE.</td>
							<td width="80">Sejahtera Bandung</td>
							
						</tr>						
						<tr>
							<td width="38">005</td>
							<td width="217">Ketua  I</td>
							<td width="255">Pdt. Charles V.J Timbuleng, M.Si</td>
							<td width="80">Bethel Bandung</td>
							
						</tr>
						<tr>
							<td width="38">006</td>
							<td width="217">Ketua  II</td>
							<td width="255">Pdt. S.G.R Sihombing, M.Th</td>
							<td width="80">Sejahtera Bandung</td>
							
						</tr>
						<tr>
							<td width="38">007</td>
							<td width="217">Ketua  III</td>
							<td width="255">Pdt. Albert Gosseling, S.Th</td>
							<td width="80">Maranatha Subang</td>
							
						</tr>
						<tr>
							<td width="38">008</td>
							<td width="217">Ketua  IV</td>
							<td width="255">Pnt. Mea Marthen Dillak</td>
							<td width="80">Maranatha Bandung</td>
							
						</tr>
						<tr>
							<td width="38">009</td>
							<td width="217">Ketua  V</td>
							<td width="255">Ny. S. Lestari Nink</td>
							<td width="80">Bethel Bandung</td>
							
						</tr>
						<tr>
							<td width="38">010</td>
							<td width="217">Ketua  VI</td>
							<td width="255">Mayor CPM Suharinto</td>
							<td width="80">Immanuel Cimahi</td>
							
						</tr>
						<tr>
							<td width="38">011</td>
							<td width="217">Ketua  VII</td>
							<td width="255">dr. Billy Agus Utomo</td>
							<td width="80">Maranatha Bandung</td>	
							
						</tr>
						<tr>
							<td width="38">012</td>
							<td width="217">SEKRETARIS UMUM</td>
							<td width="255">Pdt. Melkianus Nguru, M.Th.</td>
							<td width="80">Immanuel Cimahi</td>
							
						</tr>
						<tr>
							<td width="38">013</td>
							<td width="217">Sekretaris I</td>
							<td width="255">Pnt. Agung Suwondo</td>
							<td width="80">Maranatha Bandung</td>
							
						</tr>
						<tr>
							<td width="38">014</td>
							<td width="217">Sekretaris II</td>
							<td width="255">Pnt. Ny. Melly E. Latuheru</td>
							<td width="80">Sejahtera Bandung</td>
							
						</tr>
						<tr>
							<td width="38">015</td>
							<td width="217">BENDAHARA UMUM</td>
							<td width="255">Pnt. Eddy Sumantri</td>
							<td width="80">Bethel Bandung</td>
							
						</tr>
						<tr>
							<td width="38">016</td>
							<td width="217">Bendahara I</td>
							<td width="255">Ny. Cissie Rivai</td>
							<td width="80">Bethel Bandung</td>
						
						</tr>
						<tr>
							<td width="38">017</td>
							<td width="217">Bendahara II</td>
							<td width="255">Pnt. Hendy Sunaryo</td>
							<td width="80">Maranatha Bandung</td>
						
						</tr>
					</tbody>
					
					
					
				</table>
				
				<h3>Seksi - Seksi </h3> <br>
				
				<p> <strong>Seksi Sekretariat : <p> </strong>
				Ny. E. Hehahia &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp (Koordinator) <br>
				Pnt. Joyce Elenora J.H Toleng &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  (Wkl.Koord) <br>
				Sdri. Soelistiyana Dasmasela <br>
				Pnt Mulyani <br>
				Dkn. Alfian Bawole<br>
				Sdr. Raymond Lende<br>
				Sdr. Manta<br>
				Sdr. Abraham Simbolinggi<br>
				Sdr. Morten Jonathan<br>
				Sdr. ABrianus Musa Padademang<br>
				Bapak Lexy Yohannes<br>
				</p>
				
				<p> <strong>Seksi Acara dan Ibadah I (Konven Pendeta GPIB):<p> </strong>
				Pdt. Dr. Nancy Thelma Nisahpih - Rehatta, M.Th. &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp(Koordinator) <br>
				Pdt. Sri Butarbutar - Siregar,S.Th	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp	&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp 	(Wkl.Koord) <br>
				Pdt. Novita Rismayanti Pessireron, S.si-Teol. <br>
				Pdt. Ny.Eveline Rondo - L <br> 
				Pdt. Ferdinand I. Mamangkey, S.Th <br>
				Sdr. Yohanes Wahyu Supriadi <br>
				Sdri. Belatrik Martina Rumtotmey <br>
				</p>
				
				<p> <strong>Seksi Persidangan 	: <p> </strong>
				Pnt. Asa T.Silitonga, MBA	&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp(Koordinator)<br>
				Dr. Salmon Martana		    &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp  	(Wkl.Koord)<br>
				Pdt. Wiwik Christiani Kembuan, M.Si<br>
				Vikaris Pricilia Cindy Rosok<br>
				Vikaris Eka Rahayu Christiawan<br>
				Pnt. S.R.A Hedohari, SH.<br>
				Dkn. Socius Tutus Mangempis<br>
				Sdr. Wolter Tamahiwu<br>
				Ny. Nori Siregar- Onggeleng<br>
				Pengurus inti Pelkat GP Jabar 1<br>
				</p>
				
				
				<p> <strong>Seksi Akomodasi	: <p> </strong>
				Bapak Hayden Partono Lubis SH,Mh.	&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 			(Koordinator)<br>
				Pnt. Yohanes Salim	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 					(Wkl.Koord)<br>
				Pnt. R. Sunarto<br>
				Ny. Joice Sihasale<br>
				Pnt. Jacky A.Bernanrd<br>
				Pnt. W.Ch. H Matahelumual<br>
				Sdr. T. Wibowo<br>
				Bapak Arwin Worang<br>
				</p>
				
				
				<p> <strong>Seksi Perlengkapan & Dekorasi :<p> </strong>
				Pnt. Gustaf Komalin		&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Koordinator)<br>
				Pnt. Christian F. de Fretes	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Wkl.Koord)<br>
				Nicolas Wahyu Triyanto<br>
				Pnt. S.B.M Sianturi<br>
				Sdr. Soleman Laning<br>
				Pnt. H.H Williams<br>
				Bapak E.Silaban<br>
				Bapak Alex Z.T Watwensa<br>
				Pengurus inti Pelkat PKB Jabar 1<br>
				</p>
				
				
				<p> <strong>Seksi Humas/Publikasi/Dok :<p> </strong>
				Penatua John Tamaela		&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 				(Koordinator)<br>
				Ny. Tatya Hukom			&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp &nbsp &nbsp 			(Wkl.Koord)<br>
				Ny. Irene Tomasowa<br>
				Sdr. Andreas Tahaparry<br>
				Sdr. Julius Tomasowa<br>
				Sdr. Oktovianus Kana<br>
				</p>
				
				
				<p> <strong>Usaha Dana		:<p> </strong>
				Bapak Victor Timisela, SH	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp   &nbsp   &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Koordinator)<br>
				Ny, Anne Christina Leonard	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Wkl.Koord)<br>
				Pnt. Ny. J.R Sitorus<br>
				Pnt. Ny. Pauline Bangun<br>
				Ny. Frieda Tinggogoy<br>
				Pnt. Ny. Herlina Purba<br>
				Pnt. H.B. Damanik<br>
				Pnt. Christian H. Laihad<br>
				Sdri. Friska L. Sigalingging<br>
				Dkn. M. Nababan<br>
				Pnt. NP. Wangke.<br>
				Pnt. Widodo<br>
				Bapak Hotma Simanjuntak, SH<br>
				Pnt. Tito Sugiheryanti<br>
				</p>
				
				<p> <strong>Transportasi		:<p> </strong>
				Bapak Everd Albertus Barbier &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 					(Koordinator)<br>
				Bapak Daniel Wiray		 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Wkl.Koord)<br>
				Dkn Ericson P. Naibaho<br>
				Pnt. J.M Roemokoy<br>
				Bapak M. Sihombing<br>
				Penatua Roy G. Maringka<br>
				Sdr. Otniel Aipassa<br>
				</p>
				
				<p> <strong>Keamanan		:<p> </strong>
				Kompol Fane Lengkong, SH	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Koordinator)<br>
				Bapak Alex Darwin Lubis		 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 			(Wkl.Koord)<br>
				Bapak Stevanus Rompas<br>
				Bapak Subroto Hadi<br>
				Bapak R. B Purba<br>
				Sdr. Febby Tiwa <br>
				Bapak O. Temas<br>
				Bapak Timbul Silalahi, SH<br>
				Bapak Arnold Malonda<br>
				</p>
				
				<p> <strong>Konsumsi		:<p> </strong>
				Ny. Junny Sunardi			&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 			(Koordinator)<br>
				Ny. Etty Rukuyati Barbier	 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Wkl.Koord)<br>
				Penatua Ny. Setyati N. Luarmase<br>
				Ny. Sonya Thomas - Lumowa<br>
				Ny. Maria Salim<br>
				Ny. B.Ginting<br>
				Ny. Tommy Kawulur<br>
				Ny. Ginting Muda<br>
				Pengurus inti Pelkat PKP Jabar1<br>
				</p>
				
				<p> <strong>Kesehatan		:<p> </strong>
				Dr. Pieter Likliwatil, Sp,Bd	&nbsp &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 				(Koordinator)<br>
				Dr. Agung Sandyarso			 &nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp&nbsp &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp	 &nbsp &nbsp 			(Wkl.Koord)<br>
				Penatua Dr. Benny Kaligis<br>
				dr. Angreiny Sembiring<br>
				dr. Ferdanela Matahelumual<br>
				dr. Harry E. Saroinsong<br>
				Dkn. Ny. Hanelly Parede - T<br>
				Pengurus Balai Pengobatan Bethesda Maranatha<br>
				</p>



			</div>

			<img  style="margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_bm.png"/>
			<div class="text-content">
			
			<table style="margin-top:20px;"width="100%" >
					<tbody>
						<tr>
							<td style="text-align: left;" width="38"><strong>No</strong></td>
							<td style="text-align: left;" width="217"><strong>Jabatan</strong></td>
							<td style="text-align: left;" width="255"><strong>Nama</strong></td>
							<td style="text-align: left;" width="80"><strong>Jemaat</strong></td>
							
						</tr>
						<tr>
							<td width="38">001</td>
							<td width="217">Ketua </td>
							<td width="255">Pendeta Ebser M. Lalenoh, M.Th.</td>
							<td width="80">Maranatha Bandung</td>
						
						</tr>
						<tr>
							<td width="38">002</td>
							<td width="217">Ketua I</td>
							<td width="255">Pendeta Melkianus Nguru</td>
							<td width="80">	Immanuel Cimahi</td>
						
						</tr>
						<tr>
							<td width="38">003</td>
							<td width="217">Ketua II</td>
							<td width="255">Penatua Marthen Tapilouw</td>
							<td width="80">Bethel Bandung</td>
						
						</tr>
						<tr>
							<td width="38">004</td>
							<td width="217">Ketua III</td>
							<td width="255">Penatua Ronald K.Kumowal</td>
							<td width="80">	Sejahtera Bandung </td>
						
						</tr>
						<tr>
							<td width="38">005</td>
							<td width="217">Ketua IV</td>
							<td width="255">Penatua John Tamaela</td>
							<td width="80"> Bethel Bandung</td>
						
						</tr>
						<tr>
							<td width="38">006</td>
							<td width="217">Ketua V</td>
							<td width="255">Penatua Mea Marthen Dillak</td>
							<td width="80"> Maranatha Bandung</td>
						
						</tr>
						<tr>
							<td width="38">007</td>
							<td width="217">Sekretaris</td>
							<td width="255">Pendeta Albert Gosseling</td>
							<td width="80">Maranatha Subang</td>
						
						</tr>
						<tr>
							<td width="38">008</td>
							<td width="217">Sekretaris I</td>
							<td width="255">Penatua E.A. Latuheru - M</td>
							<td width="80">Sejahtera Bandung</td>
						
						</tr>
						<tr>
							<td width="38">009</td>
							<td width="217">Bendahara</td>
							<td width="255">Penatua Widodo</td>
							<td width="80">Maranatha Bandung</td>
						
						</tr>
						<tr>
							<td width="38">010</td>
							<td width="217">Bendahara I</td>
							<td width="255">Penatua Yohanes Salim</td>
							<td width="80">Maranatha Bandung</td>
						
						</tr>
					</tbody>
				</table>
			</div>
			
			<img  style="margin-left:20px;" src="<?php echo base_url()?>public/img/web/t_kp.png"/>
			<div class="text-content">
				<p>
					Panitia Pelaksana Konven Pendeta dan Persidangan Sinode Tahunan GPIB<br />
					Sekretariat: GPIB Bethel Bandung, Jalan Wastukencana nomor 1, Bandung<br />
					Telp. 022-426970073
					Pdt. Melkianus Nguru, M.Th.		/ Hp.081342309992
					Pnt. Agung Suwondo 				/ Hp.089699637447
					Pnt. Ny. Melly E. Latuheru   	/ Hp.0811237147
				</p>
				
				<p>
					E-Mail :<br />
					<a href="#">panitiakonvenpst2016@yahoo.com</a><br />
					
				</p>
			</div>
						
		</div>
		
		<div class="right_menu">
		<div class="content_menu">
				<div class="menu_header">Menu</div>
				<div class="menu_content">
					<ul>
					<?php if (!$already_login) {
						  echo "<center><li><a style='font-size:1.4em;' href='".base_url()."login' class='link'>Login</a></li>	</center>";
					  } else {
              echo "<center><li><a style='font-size:1.4em;' href='".base_url()."logout' class='link'>Logout</a></li>  </center>";
            } ?>
					</ul>

				</div>
			</div>
			<div class="content_menu">
			<center>
				<div class="menu_header">Media Sosial</div>
				<div class="menu_content">
				<img onclick="location.href='https://www.facebook.com/profile.php?id=100009224248313'" style ="padding-left:-20px;width:180px; height:180px;" src="<?php echo base_url()?>public/img/web/fb.jpg"/>
				</div>
			</center>
			</div>
		</div>
		
		
	</div>

    
</body>
</html>