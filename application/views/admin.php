<!DOCTYPE html>
<html>
<head>
	<title>PST GPIB</title>
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox, .form-signin .recover {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin #inputCode {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
		
    </style>
</head>
<body>
	<div class="container">
      <?php if ($a_type != null) {
        echo "<div class='alert alert-$a_type' role='alert'><strong>$a_title</strong> $a_body</div>";
      } ?>

      <form class="form-signin" method="post" action="admin">
        <h2 class="form-signin-heading">Lengkapi Data Anda</h2>
        <label for="inputEmail" class="sr-only">Alamat Email</label>
        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Alamat Email" required autofocus>
        
        <label for="inputName" class="sr-only">Nama</label>
        <input type="text" id="inputName" name="inputName" class="form-control" placeholder="Nama" required autofocus>
        
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
        
        <label for="inputConfPassword" class="sr-only">Konfirmasi Password</label>
        <input type="password" id="inputConfPassword" name="inputConfPassword" class="form-control" placeholder="Konfirmasi Password" required>

        <label for="inputCode" class="sr-only">Kode Unik</label>
        <input type="text" id="inputCode" name="inputCode" class="form-control" placeholder="Kode Unik" value="<?php echo $code ?>"required>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign Up</button>
      </form>

    </div> <!-- /container -->
</body>
</html>