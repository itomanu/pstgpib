<!DOCTYPE html>
<html>
<head>
  <title>PST GPIB</title>
  <link rel="shortcut icon" href="<?php echo base_url()?>public/img/web/logo.jpg"/>
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-login {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-login .form-login-heading,
.form-login .checkbox, .form-login .recover {
  margin-bottom: 10px;
}
.form-login .checkbox {
  font-weight: normal;
}
.form-login .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-login .form-control:focus {
  z-index: 2;
}
.form-login input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-login #inputConfPassword {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
    
    </style>
</head>
<body>
  <div class="container">
      <?php if ($a_type != null) {
        echo "<div class='alert alert-$a_type' role='alert'><strong>$a_title</strong> $a_body</div>";
      } ?>

      <form class="form-login" method="post" action="login">
        <h2 class="form-login-heading">Login</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Login</button>
      </form>

    </div> <!-- /container -->
</body>
</html>