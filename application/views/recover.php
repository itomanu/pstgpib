<!DOCTYPE html>
<html>
<head>
	<title>PST GPIB</title>
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-recover, .boundary {
  max-width: 330px;
  margin: 0 auto;
  padding: 15px;
}

.boundary {
  padding-bottom: 0;
}
.form-recover .form-recover-heading,
.form-recover .checkbox, .form-recover .recover {
  margin-bottom: 10px;
}
.form-recover .checkbox {
  font-weight: normal;
}
.form-recover .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-recover .form-control:focus {
  z-index: 2;
}
.form-recover input[type="email"] {
  margin-bottom: 10px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
		
    </style>
</head>
<body>
	<div class="container">
    <div class="boundary">
      <h2 class="form-recover-heading">Account Recovery</h2>

      <?php
        if( isset( $disabled ) )
        {
          echo '
            <div class="alert alert-danger" role="alert">
              <p><strong>Account Recovery is Disabled.</strong></p>
              <p>
                If you have exceeded the maximum login attempts, or exceeded
                the allowed number of password recovery attempts, account recovery 
                will be disabled for a short period of time. 
                Please wait ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' 
                minutes, or contact us if you require assistance gaining access to your account.
              </p>
            </div>
          ';
        }
        else if( isset( $user_banned ) )
        {
          echo '
            <div class="alert alert-danger" role="alert">
              <p><strong>Account Locked.</strong></p>
              <p>
                You have attempted to use the password recovery system using 
                an email address that belongs to an account that has been 
                purposely denied access to the authenticated areas of this website. 
                If you feel this is an error, you may contact us  
                to make an inquiry regarding the status of the account.
              </p>
            </div>
          ';
        }
        else if( isset( $confirmation ) )
        {
          echo '
            <div class="alert alert-success" role="alert">
              <p><strong>Check your Email.</strong></p>
              <p>
                We have sent you an email with instructions on how 
                to recover your account.
              </p>
            </div>
            <a class="btn btn-lg btn-primary btn-block" href="'.base_url().'login" role="button">Go To Login</a>
          ';
        }
        else if( isset( $no_match ) )
        {
          echo '
            <div class="alert alert-danger" role="alert">
              <p class="feedback_header">
                Supplied email did not match any record.
              </p>
            </div>
          ';

          $show_form = 1;
        }
        else
        {
          echo '
            <div class="alert alert-info" role="alert">
              <p>
                If you\'ve forgotten your password and/or username, 
                enter the email address used for your account, 
                and we will send you an e-mail 
                with instructions on how to access your account.
              </p>
            </div>
          ';

          $show_form = 1;
        }?>

    </div>
<?php
    if( isset( $show_form ) )
{
  ?>
  <?php echo form_open( '' , array('class'=>'form-recover')); ?>

      <label for="user_email" class="sr-only">Email address</label>
      <input type="email" id="user_email" name="user_email" class="form-control" placeholder="Email address" required autofocus>
      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Send Email</button>
    </form>
  <?php
}?>
  </div> <!-- /container -->
</body>
</html>